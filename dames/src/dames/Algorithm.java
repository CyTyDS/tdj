package dames;

import java.util.HashMap;
import java.util.LinkedList;

public abstract class Algorithm {
    private GameModel board;
    private int rootColor;
    private int prof;
    private HashMap<GameModel, LinkedList<GameModel>> tree;

    public Algorithm(GameModel b, int prof) {
    	this.prof = prof;
        board = b;
        rootColor = b.getCurrentPlayer();
        tree = new HashMap<GameModel, LinkedList<GameModel>>();
    }

    public GameModel getBoard() {
        return board;
    }

    public HashMap<GameModel, LinkedList<GameModel>> getTree() {
        return tree;
    }

    public int calculatePieceAttackHeuristic(Piece piece) {
        LinkedList<AttackMovement> ams = piece.getAttack();
        int heur = 0;
        for (AttackMovement am : ams) {
            if (piece.getColor() == Piece.WHITE && board.getCurrentPlayer() == Piece.WHITE) {
                heur += am.getTarget().getX() + 1;
            } else if (piece.getColor() == Piece.BLACK && board.getCurrentPlayer() == Piece.BLACK) {
                heur += GameModel.MAX_X - am.getTarget().getX() + 1;
            }
        }
        return heur;
    }

    public int calculateBlackHeuristic() {
        board.allMovement();
        LinkedList<Piece> pieces = board.getPiecesByColor(Piece.BLACK);
        int heuristic = 0;
        int heuristicb = 0;
        for (Piece piece : pieces) {
            heuristic += piece.getX() + 1;
            heuristicb += calculatePieceAttackHeuristic(piece);
        }
        if (heuristic == 0) {
            return -10000;
        }
        heuristic += heuristicb;

        pieces = board.getPiecesByColor(Piece.WHITE);
        for (Piece piece : pieces) {
            heuristic -= GameModel.MAX_X - piece.getX() + 1;
        }
        return heuristic - heuristicb;
    }

    public int calculateWhiteHeuristic() {
        board.allMovement();
        LinkedList<Piece> pieces = board.getPiecesByColor(Piece.WHITE);
        int heuristic = 0;
        int heuristicw = 0;
        for (Piece piece : pieces) {
            heuristic += GameModel.MAX_X - piece.getX() + 1;
            heuristicw += calculatePieceAttackHeuristic(piece);
        }
        if (heuristic == 0) {
            return -10000;
        }
        heuristic += heuristicw;

        pieces = board.getPiecesByColor(Piece.BLACK);
        for (Piece piece : pieces) {
            heuristic -=  piece.getX() + 1;
        }
        return heuristic - heuristicw;
    }

    public int calculateHeuristic() {
        int heuristic = (board.getCurrentPlayer() == Piece.WHITE) ?
            calculateWhiteHeuristic() :
            calculateBlackHeuristic();
        return (board.getCurrentPlayer() == rootColor) ? heuristic : -heuristic;
    }

    // public 

    abstract void play();

	public int getProf() {
		return prof;
	}

	public void setProf(int prof) {
		this.prof = prof;
	}

	public int getRootColor() {
		return rootColor;
	}

	public void setRootColor(int rootColor) {
		this.rootColor = rootColor;
	}
}
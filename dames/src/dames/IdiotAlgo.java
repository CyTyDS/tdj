package dames;

import java.util.LinkedList;



public class IdiotAlgo extends Algorithm {
    private Movement movement;
    private int valueMovement;
	
	public IdiotAlgo(GameModel b, int prof) {
        super(b, prof);
        movement = null;
        valueMovement = 0;
	}

    private void doAttackPath() {
        LinkedList<AttackMovement> attmov = getBoard().getAttackPaths().getFirst();
        while(!attmov.isEmpty()) {
            AttackMovement mov = attmov.getFirst();
            getBoard().getBoard()[mov.getBeginX()][mov.getBeginY()].doMove(mov);
            attmov.removeFirst();
        }
    }

	@Override
	public void play() {
        getBoard().optimalPath();
        LinkedList<LinkedList<AttackMovement>> attmov = getBoard().getAttackPaths();
        if (!attmov.isEmpty()) {
            LinkedList<AttackMovement> attaque = attmov.get((int) (Math.random() * attmov.size()));
            getBoard().setAttackPath(attaque);
            doAttackPath();
            getBoard().getAttackPaths().clear();
        } else {
            LinkedList<Piece> pieces = getBoard().getPiecesByColor(getBoard().getCurrentPlayer());
            for (Piece piece : pieces) {
                LinkedList<Movement> moves = piece.getMove();
                for (Movement move : moves) {
                    piece.doMove(move);
                    getBoard().changeCurrentPlayer();
                    int heur = -calculateHeuristic();
                    getBoard().changeCurrentPlayer();
                    if (movement == null) {
                        movement = move;
                        valueMovement = heur;
                    } else {
                        if (heur > valueMovement) {
                            movement = move;
                            valueMovement = heur;
                        } else if (heur == valueMovement) {
                            if (Math.random() > 0.5) {
                                valueMovement = heur;
                                movement = move;
                            }
                        }
                    }
                    getBoard().movePiece(piece, move.getBeginX(), move.getBeginY());
                }
            }
            getBoard().movePiece(getBoard().getBoard()[movement.getBeginX()][movement.getBeginY()], movement.getEndX(), movement.getEndY());
        }
    }
}

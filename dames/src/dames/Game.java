package dames;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.ImageIcon;

public class Game {
	// ATTRIBUTS
	private JFrame mainFrame;
	private GameModel model;
    private JMenuBar menuBar;
    private JMenu[] menu;
    private JMenuItem np;
    private JLabel[][] cases;
	private JPanel plat;
	private Map<JLabel, int[]> labelPos;
	private Piece selected;
	private HashMap<JLabel,Movement> landing;
	private HashMap<JLabel,AttackMovement> attack;
	private String algo1;
	private String algo2;
	
	public Game() {
        createModel();
        createView();
        placeComponents();
        createController();
	}

    public void display() {
        refresh();
        mainFrame.pack();
        mainFrame.setLocationRelativeTo(null);
		mainFrame.setVisible(true);
		
		choseAlgos();
		// if (algo2 != "joueur") {
		// 	model.changeCurrentPlayer();
		// 	while (model.getPiecesByColor(0).size() != 0 || model.getPiecesByColor(1).size() != 0) {
		// 		algoMove((model.getCurrentPlayer() + 1) % 2);
		// 		model.changeCurrentPlayer();
		// 		refresh();
		// 		try {
		// 			Thread.sleep(1500);
		// 		} catch (Exception e) {
		// 		}
		// 	}
		// }
    }
	
	private void choseAlgos() {
		String choix[] = {"AlphaBeta", "NegAlphaBeta", "Sss*", "Idiot"};
		int reponse = JOptionPane.showOptionDialog(null, 
			"quelle dificultée (algorithme de résolution) voulez vous pour les noirs?", 
			"choix",
			JOptionPane.DEFAULT_OPTION,
			JOptionPane.INFORMATION_MESSAGE, null, choix, choix[0]);

		if (reponse != JOptionPane.CLOSED_OPTION) {
			switch(reponse) {
				case 0: algo1 = "alphabeta";break;
				case 1: algo1 = "negalphabeta";break;
				case 2: algo1 = "sss*";break;
				case 3: algo1 = "Idiot";break;
			}
		}

		// String choix2[] = {"joueur", "AlphaBeta", "NegAlphaBeta", "Sss*", "Idiot"};
		// int reponse2 = JOptionPane.showOptionDialog(null, 
		// 	"quelle dificultée (algorithme de résolution) voulez vous pour les blancs?", 
		// 	"choix",
		// 	JOptionPane.DEFAULT_OPTION,
		// 	JOptionPane.INFORMATION_MESSAGE, null, choix2, choix2[0]);

		// if (reponse2 != JOptionPane.CLOSED_OPTION) {
		// 	switch(reponse2) {
		// 		case 0: algo2 = "joueur";break;
		// 		case 1: algo2 = "alphabeta";break;
		// 		case 2: algo2 = "negalphabeta";break;
		// 		case 3: algo2 = "sss*";break;
		// 		case 4: algo2 = "Idiot";break;
		// 	}
		// }
	}

    private void createModel() {
		model = new GameModel();
		model.fillBoard();
    }
    
    private void createView() {
        mainFrame = new JFrame("dames AGULLO CHAURIN SAUDE");

        menuBar = new JMenuBar();
        menu = new JMenu[1];
        menu[0] = new JMenu("Menu");
        
        np = new JMenuItem("Nouvelle Partie");
        
        menu[0].add(np);

		cases = new JLabel[10][10];
		plat = new JPanel();
        plat.setLayout(new GridLayout(10,10));
            
		labelPos = new HashMap<JLabel, int[]>();

        for (int x = 0; x <= GameModel.MAX_X; x++) {
            for (int y = 0; y <= GameModel.MAX_Y; y++) {
				cases[x][y] = new JLabel();
				int[] pos = {x, y};
				labelPos.put(cases[x][y], pos);
                cases[x][y].setBackground(((x + y) % 2 == 0) ? Color.WHITE : Color.GRAY);
				cases[x][y].setOpaque(true);
				cases[x][y].setPreferredSize(new Dimension(85,85));
				plat.add(cases[x][y]);
            }
		}
		
		selected = null;
		landing = new HashMap<JLabel,Movement>();
		attack = new HashMap<JLabel,AttackMovement>();
    }

    private void placeComponents() {
    	menuBar.add(menu[0]);
    	mainFrame.add(menuBar, BorderLayout.NORTH);
    	mainFrame.add(plat, BorderLayout.CENTER);
    }
    
    private void createController() {
    	mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    	
    	mainFrame.addWindowListener(new WindowAdapter() {
    		public void windowClosing(WindowEvent e) {
    			int reponse = JOptionPane.showConfirmDialog(null, 
    					"Voules vous vraiment quitter?", 
    					"Quitter", JOptionPane.YES_NO_OPTION);
    			if(reponse == JOptionPane.YES_OPTION) {
    				mainFrame.dispose();
    			}
    		}
    	});
    	
    	np.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				createModel();
				choseAlgos();
				refresh();
			}
    	});

		for (int x = 0; x <= GameModel.MAX_X; x++) {
			for (int y = 0; y <= GameModel.MAX_Y; y++) {
    			addMouseListenerToLabel(cases[x][y]);
			}
		}
	}
	
	private void algoMove(int algo) {
		if (algo == 1) {
			if (algo1 == "alphabeta") {
				model.changeCurrentPlayer();
				AlphaBetaAlgo a = new AlphaBetaAlgo(model, 3);
				a.play();
				model = a.getBestBoard();
			} else if (algo1 == "Idiot") {
				model.changeCurrentPlayer();
				IdiotAlgo i = new IdiotAlgo(model, 0);
				i.play();
				model.changeCurrentPlayer();
			} else {
				model.changeCurrentPlayer();
			}
		} else {
			if (algo2 == "alphabeta") {
				model.changeCurrentPlayer();
				AlphaBetaAlgo a = new AlphaBetaAlgo(model, 3);
				a.play();
				model = a.getBestBoard();
			} else if (algo2 == "Idiot") {
				model.changeCurrentPlayer();
				IdiotAlgo i = new IdiotAlgo(model, 0);
				i.play();
				model.changeCurrentPlayer();
			} else {
				model.changeCurrentPlayer();
			}
		}
	}

	private void addMouseListenerToLabel(JLabel lab) {
		lab.addMouseListener(new MouseAdapter() {
        	
			public void mousePressed(MouseEvent e) {
				if (!attack.isEmpty()) {
					AttackMovement atkmov = attack.get(e.getSource());
					if (atkmov != null) {
						Piece attacker = model.getPieceAtPosition(atkmov.getBeginX(), atkmov.getBeginY());
						attacker.doMove(atkmov);
						if (model.getPiecesByColor((attacker.getColor() + 1) % 2).isEmpty()) {
							JOptionPane.showMessageDialog(null, 
							"le joueur " + (attacker.getColor() == 0 ? "blanc" : "noir") + " a gagné",
							" victoire ",
							JOptionPane.WARNING_MESSAGE);
							return;
						}
						if (attacker.getColor() == Piece.BLACK && attacker.getX() == 9 || attacker.getColor() == Piece.WHITE && attacker.getX() == 0) {
							attacker = model.menToKing(attacker);
						}

						// attacker.possibleMovements();
						// if (attacker.getAttack().isEmpty()) {
						// 	model.changeCurrentPlayer();
						// }

						model.optimalPath(attacker);
						if (model.getAttackPaths().isEmpty()) {
							algoMove(1);
						}

						calculateNextMoves();
						refresh();
						return;
					} else {
						return;
					}
				}
				int[] pos = labelPos.get(e.getSource());
				Piece piece = model.getPieceAtPosition(pos[0], pos[1]);
				if (piece != null && piece.getColor() == model.getCurrentPlayer()) {
					selected = piece;
					piece.possibleMovements();
					LinkedList<Movement> moves = piece.getMove();
					landing.clear();
					for (Movement mov : moves) {
						landing.put(cases[mov.getEndX()][mov.getEndY()], mov);
					}
				} else {
					Movement mov = landing.get(e.getSource());
					if (mov != null) {
						selected.doMove(mov);
						if (selected.getColor() == Piece.BLACK && selected.getX() == 9 || selected.getColor() == Piece.WHITE && selected.getX() == 0) {
							selected = model.menToKing(selected);
						}
						algoMove(1);
						calculateNextMoves();
						selected = null;
					}
				}
				refresh();
			}
		});
	}
	
	private void calculateNextMoves() {
		landing.clear();
		attack.clear();
		LinkedList<LinkedList<AttackMovement>> paths = model.getAttackPaths();
		if (!paths.isEmpty()) {
			for (LinkedList<AttackMovement> lists : paths) {
				model.getPieceAtPosition(lists.getFirst().getBeginX(), lists.getFirst().getBeginY()).possibleMovements();
				attack.put(cases[lists.getFirst().getEndX()][lists.getFirst().getEndY()], lists.getFirst());
			}
		} else {
			model.optimalPath();
			LinkedList<LinkedList<AttackMovement>> scdPaths = model.getAttackPaths();
			if (!scdPaths.isEmpty()) {
				for (LinkedList<AttackMovement> lists : scdPaths) {
					model.getPieceAtPosition(lists.getFirst().getBeginX(), lists.getFirst().getBeginY()).possibleMovements();
					attack.put(cases[lists.getFirst().getEndX()][lists.getFirst().getEndY()], lists.getFirst());
				}
			}
		}
	}

    private void refresh() {
		for (int x = 0; x <= GameModel.MAX_X; x++) {
			for (int y = 0; y <= GameModel.MAX_Y; y++) {
                cases[x][y].setBackground(((x + y) % 2 == 0) ? Color.WHITE : Color.GRAY);
				Piece piece = model.getPieceAtPosition(x, y);
				if (piece != null) {
					if (piece instanceof Men) {
						cases[x][y].setIcon(new ImageIcon(piece.getColor() == Piece.WHITE ? "whiteMen.png" : "blackMen.png"));
					} else {
						cases[x][y].setIcon(new ImageIcon(piece.getColor() == Piece.WHITE ? "whiteKing.png" : "blackKing.png"));
					}
				} else {
					cases[x][y].setIcon(null);
					cases[x][y].setText(x+","+y);
				}
			}
		}
		if (!landing.isEmpty()) {
			for (JLabel lab : landing.keySet()) {
				lab.setBackground(Color.YELLOW);
			}
		} else if (!attack.isEmpty()) {
			for (JLabel lab : attack.keySet()) {
				lab.setBackground(Color.RED);
			}
		}
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Game().display();
            }
        });
    }
}
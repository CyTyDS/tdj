package dames;

public class King extends Piece {

	public King(int col, int posx, int posy, GameModel b) throws Exception {
		super(col, posx, posy, b);
	}
	
	public void possibleMovements() {
		getAttack().clear();
		getMove().clear();
		int targetX = getX() + 1;
		int targetY = getY() + 1;
		while (getBoard().isLegalPosition(targetX, targetY)) {
			Piece piece = getBoard().getPieceAtPosition(targetX, targetY);
			if (piece != null) {
				if (canAttack(piece)) {
					fillAttack(piece);
				} else {
					break;
				}
			} else if (canMove(targetX, targetY)) {
				getMove().add(new Movement(getX(),getY(),targetX,targetY));
			}
			targetX++;
			targetY++;
		}

		targetX = getX() + 1;
		targetY = getY() - 1;
		while (getBoard().isLegalPosition(targetX, targetY)) {
			Piece piece = getBoard().getPieceAtPosition(targetX, targetY);
			if (piece != null) {
				if (canAttack(piece)) {
					fillAttack(piece);
				} else {
					break;
				}
			} else if (canMove(targetX, targetY)) {
				getMove().add(new Movement(getX(),getY(),targetX,targetY));
			}
			targetX++;
			targetY--;
		}
		targetX = getX() - 1;
		targetY = getY() - 1;
		while (getBoard().isLegalPosition(targetX, targetY)) {
			Piece piece = getBoard().getPieceAtPosition(targetX, targetY);
			if (piece != null) {
				if (canAttack(piece)) {
					fillAttack(piece);
				} else {
					break;
				}
			} else if (canMove(targetX, targetY)) {
				getMove().add(new Movement(getX(),getY(),targetX,targetY));
			}
			targetX--;
			targetY--;
		}
		targetX = getX() - 1;
		targetY = getY() + 1;
		while (getBoard().isLegalPosition(targetX, targetY)) {
			Piece piece = getBoard().getPieceAtPosition(targetX, targetY);
			if (piece != null) {
				if (canAttack(piece)) {
					fillAttack(piece);
				} else {
					break;
				}
			} else if (canMove(targetX, targetY)) {
				getMove().add(new Movement(getX(),getY(),targetX,targetY));
			}
			targetX--;
			targetY++;
		}
	}
	
	public boolean canAttack(Piece p) {
		int directionX = p.getX() - getX();
		int directionY = p.getY() - getY();

		directionX /= Math.abs(directionX);
		directionY /= Math.abs(directionY);

		if (!getBoard().isLegalPosition(p.getX() + directionX, p.getY() + directionY) || 
			getBoard().getPieceAtPosition(p.getX() + directionX, p.getY() + directionY) != null ||
			p.getColor() == getColor()) {
			return false;
		}

		int interposX = getX() + directionX;
		int interposY = getY() + directionY;
		while (interposX != p.getX() && interposY != p.getY()) {
			if (!getBoard().isLegalPosition(interposX, interposY)) {
				return false;
			}
			if (getBoard().getPieceAtPosition(interposX, interposY) != null) {
				return false;
			}
			interposX = interposX + directionX;
			interposY = interposY + directionY;
		}

		return true;
		/*boolean boolHG = true;
		boolean boolHD = true;
		boolean boolBG = true;
		boolean boolBD = true;

		int cpt = 1;
		while (boolHG || boolHD || boolBG || boolBD) {
			if(boolBD) {
				int newx = getX() + cpt;
				int newy = getY() + cpt;
				if (getBoard().isLegalPosition(newx, newy)) {
					if (p.getX() == newx && p.getY() == newy) {
						if (getBoard().isLegalPosition(newx + 1, newy + 1) && getBoard().getPieceAtPosition(newx + 1, newy + 1) == null) {
							return true;
						} else {
							boolBD = false;
						}
					}
				} else {
					boolBD = false;
				}
			}
			if(boolBG) {
				int newx = getX() - cpt;
				int newy = getY() + cpt;
				if (getBoard().isLegalPosition(newx, newy)) {
					if (p.getX() == newx && p.getY() == newy) {
						if (getBoard().isLegalPosition(newx - 1, newy + 1) && getBoard().getPieceAtPosition(newx - 1, newy + 1) == null) {
							return true;
						} else {
							boolBG = false;
						}
					}
				} else {
					boolBG = false;
				}
			}
			if(boolHG) {
				int newx = getX() - cpt;
				int newy = getY() - cpt;
				if (getBoard().isLegalPosition(newx, newy)) {
					if (p.getX() == newx && p.getY() == newy) {
						if (getBoard().isLegalPosition(newx - 1, newy - 1) && getBoard().getPieceAtPosition(newx - 1, newy - 1) == null) {
							return true;
						} else {
							boolHG = false;
						}
					}
				} else {
					boolHG = false;
				}
			}
			if(boolHD) {
				int newx = getX() + cpt;
				int newy = getY() - cpt;
				if (getBoard().isLegalPosition(newx, newy)) {
					if (p.getX() == newx && p.getY() == newy) {
						if (getBoard().isLegalPosition(newx + 1, newy - 1) && getBoard().getPieceAtPosition(newx + 1, newy - 1) == null) {
							return true;
						} else {
							boolHD = false;
						}
					}
				} else {
					boolHD = false;
				}
			}
			return false;
		}*/
	}
	
	public boolean canMove(int posX, int posY) {
		int directionX = posX - getX();
		int directionY = posY - getY();

		directionX /= Math.abs(directionX);
		directionY /= Math.abs(directionY);

		if (!getBoard().isLegalPosition(posX, posY) || getBoard().getPieceAtPosition(posX, posY) != null) {
			return false;
		}

		int cpt = 1;
		int interposX = getX() + cpt * directionX;
		int interposY = getY() + cpt * directionY;
		while (interposX != posX && interposY != posY) {
			if (!getBoard().isLegalPosition(interposX, interposY)) {
				return false;
			}
			if (getBoard().getPieceAtPosition(interposX, interposY) != null) {
				return false;
			}
			cpt++;
			interposX = getX() + cpt * directionX;
			interposY = getY() + cpt * directionY;
		}

		return true;
	}

	private void fillAttack(Piece p) {
		int directionX = p.getX() - getX();
		int directionY = p.getY() - getY();

		directionX /= Math.abs(directionX);
		directionY /= Math.abs(directionY);

		int interX = p.getX() + directionX;
		int interY = p.getY() + directionY;
		while (getBoard().isLegalPosition(interX, interY) && getBoard().getPieceAtPosition(interX, interY) == null) {
			getAttack().add(new AttackMovement(getX(),getY(),interX,interY,p));
			interX = interX + directionX;
			interY = interY + directionY;
		}
	}

	/*
		while (boolHG || boolHD || boolBG || boolBD) {
			if(boolBD) {
				int newx = getX() + cpt;
				int newy = getY() + cpt;
				if (getBoard().isLegalPosition(newx, newy)) {
					if (p.getX() == newx && p.getY() == newy) {
						int cmpt = 1;
						while (getBoard().isLegalPosition(newx + cmpt, newy + cmpt) && getBoard().getPieceAtPosition(newx + cmpt, newy + cmpt) == null) {
							
						}
					}
				}
			}
		}
		*/
}

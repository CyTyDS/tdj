package dames;

public class AttackMovement extends Movement{
    private Piece target;
    
	public AttackMovement(int bX, int bY, int eX, int eY, Piece targ) {
        super(bX, bY, eX, eY);
        target = targ;
	}
    
    public Piece getTarget() {
        return target;
    }

    public void undoMove() {
        GameModel board = target.getBoard();
        Piece piece = board.getPieceAtPosition(getEndX(), getEndY());
        board.movePiece(piece, getBeginX(), getBeginY());
        board.movePiece(target, target.getX(), target.getY());
    }

    public AttackMovement clone() {
        return new AttackMovement(getBeginX(), getBeginY(), getEndX(), getEndY(), getTarget());
    }
}

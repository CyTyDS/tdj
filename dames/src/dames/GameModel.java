package dames;

import java.util.LinkedList;

public class GameModel {
	static int MAX_X = 9;
	static int MAX_Y = 9;
	static int MIN_X = 0;
	static int MIN_Y = 0;
	
	private Piece[][] board;		// 0,0 == en haut à gauche
	private int currentPlayer;
	private LinkedList<LinkedList<AttackMovement>> attackPaths;
	
	public GameModel() {
		board = new Piece[MAX_X + 1][MAX_Y + 1];
		for (int x = MIN_X; x <= MAX_X; x++) {
			for (int y = MIN_Y; y <= MAX_Y; y++) {
				board[x][y] = null;
			}
		}
		currentPlayer = Piece.WHITE;
		attackPaths = new LinkedList<LinkedList<AttackMovement>>();
	}
	
	public GameModel(Piece[][] b, int currentPlayer, LinkedList<LinkedList<AttackMovement>> attackPaths) {
		this.board = b;
		this.currentPlayer = currentPlayer;
		this.attackPaths = attackPaths;
	}
	
	public Piece[][] getBoard() {
		return board;
	}

	public Piece getPieceAtPosition(int x, int y) {
		return board[x][y];
	}

	public LinkedList<Piece> getPiecesByColor(int col) {
		LinkedList<Piece> pieces = new LinkedList<Piece>();
		for (int x = MIN_X; x <= MAX_X; x++) {
			for (int y = MIN_Y; y <= MAX_Y; y++) {
				if (board[x][y] != null && board[x][y].getColor() == col) {
					pieces.add(board[x][y]);
				}
			}
		}
		return pieces;
	}

	public int getCurrentPlayer() {
		return currentPlayer;
	}

	public LinkedList<LinkedList<AttackMovement>> getAttackPaths() {
		return attackPaths;
	}

	public void changeCurrentPlayer() {
		currentPlayer = (currentPlayer + 1) % 2;
	}

	public void deletePieceAtPosition(int x, int y) {
		if (isLegalPosition(x,y)) {
			board[x][y] = null;
		}
	}

	public void movePiece(Piece p, int x, int y) {
		if (isLegalPosition(x,y)) {
			deletePieceAtPosition(p.getX(), p.getY());
			board[x][y] = p;
			p.setX(x);
			p.setY(y);
		}
	}

	public void fillBoard() {
		for (int x = MIN_X; x < 3; x++) {
			for (int y = MIN_Y; y <= MAX_Y; y++) {
				int xy = x + y;
				if ((xy % 2) == 1) {
					board[x][y] = new Men(Piece.BLACK, x, y, this);
				}
			}
		}

		for (int x = 7; x <= MAX_X; x++) {
			for (int y = MIN_Y; y <= MAX_Y; y++) {
				int xy = x + y;
				if ((xy % 2) == 1) {
					board[x][y] = new Men(Piece.WHITE, x, y, this);
				}
			}
		}
	}
	
	public boolean isLegalPosition(int x, int y) {
		return x <= MAX_X && MIN_X <= x && y <= MAX_Y && MIN_Y <= y;
	}

	public void allMovement() {
		for (int x = MIN_X; x <= MAX_X; x++) {
			for (int y = MIN_Y; y <= MAX_Y; y++) {
				if (board[x][y] != null && board[x][y].getColor() == currentPlayer) {
					board[x][y].possibleMovements();
				}
			}
		}
	}

	public LinkedList<Piece> getThoseWithAttack() {
		LinkedList<Piece> pieces = new LinkedList<Piece>();
		for (int x = MIN_X; x <= MAX_X; x++) {
			for (int y = MIN_Y; y <= MAX_Y; y++) {
				Piece piece = board[x][y];
				if (piece != null && piece.getColor() == currentPlayer && !piece.getAttack().isEmpty()) {
					pieces.add(piece);
				}
			}
		}
		return pieces;
	}

	public King menToKing(Piece p) {
		King k = null;
		try {
			k = new King(p.getColor(), p.getX(), p.getY(), this);
		} catch (Exception e) {
			return null;
		}
		board[p.getX()][p.getY()] = k;
		return k;
	}

	public LinkedList<AttackMovement> cloneMoveList(LinkedList<AttackMovement> list) {
		LinkedList<AttackMovement> clone = new LinkedList<AttackMovement>();
		for (AttackMovement move : list) {
			clone.add(move.clone());
		}
		return clone;
	}

	public void optimalPath(Piece piece) {
		attackPaths.clear();
		optimalPath(piece, new LinkedList<AttackMovement>());
	}

	private void optimalPath(Piece piece, LinkedList<AttackMovement> actualPath) {
		piece.possibleMovements();
		if (piece.getAttack().isEmpty()) {
			if (attackPaths.isEmpty()) {
				if (!actualPath.isEmpty())
					attackPaths.add(cloneMoveList(actualPath));
			} else if (actualPath.size() > attackPaths.getFirst().size()) {
				attackPaths.clear();
				attackPaths.add(cloneMoveList(actualPath));
			} else if (actualPath.size() == attackPaths.getFirst().size()) {
				attackPaths.add(cloneMoveList(actualPath));
			}
		} else {
			LinkedList<AttackMovement> atts = cloneMoveList(piece.getAttack());
			for(AttackMovement att : atts) {
				piece.doMove(att);
				actualPath.add(att);
				optimalPath(piece, actualPath);
				att.undoMove();
				actualPath.remove(att);
			}
		}
	}

	public LinkedList<LinkedList<AttackMovement>> cloneMoveListList(LinkedList<LinkedList<AttackMovement>> list) {
		LinkedList<LinkedList<AttackMovement>> clone = new LinkedList<LinkedList<AttackMovement>>();
		for (LinkedList<AttackMovement> simpleList : list) {
			clone.add(cloneMoveList(simpleList));
		}
		return clone;
	}

	public void optimalPath() {
		allMovement();
		LinkedList<LinkedList<AttackMovement>> paths = new LinkedList<LinkedList<AttackMovement>>();
		LinkedList<Piece> attackers = getThoseWithAttack();
		for (Piece piece : attackers) {
			optimalPath(piece);
			if (!attackPaths.isEmpty()) {
				if (paths.size() == 0 || attackPaths.getFirst().size() > paths.getFirst().size()) {
					paths = cloneMoveListList(attackPaths);
				} else if (attackPaths.getFirst().size() == paths.getFirst().size()) {
					for (LinkedList<AttackMovement> llam : attackPaths) {
						paths.add(llam);	//verif si besoin de clone
					}
				}
			}
		}
		attackPaths = paths;
	}
	
	public GameModel clone() {
		return new GameModel(this.getBoard().clone(), this.getCurrentPlayer(), (LinkedList<LinkedList<AttackMovement>>) this.cloneMoveListList(this.getAttackPaths()));
	}

	static LinkedList<Movement> cloneMovementList(LinkedList<Movement> list) {
		LinkedList<Movement> clone = new LinkedList<Movement>();
		for (Movement move : list) {
			clone.add(move.clone());
		}
		return clone;
	}

	protected void setAttackPath(LinkedList<AttackMovement> attmov) {
		attackPaths.clear();
		attackPaths.add(attmov);
	}
}

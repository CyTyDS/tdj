package dames;

public class Men extends Piece {

	public Men(int col, int posx, int posy, GameModel b) {
		super(col, posx, posy, b);
	}
	
	public void possibleMovements() {
		getAttack().clear();
		getMove().clear();
		int targetX = getX() + 1;
		int targetY = getY() + 1;
		if (getBoard().isLegalPosition(targetX, targetY)) {
			Piece piece = getBoard().getPieceAtPosition(targetX, targetY);
			if (piece != null) {
				if (canAttack(piece)) {
					getAttack().add(new AttackMovement(getX(),getY(),getX()+2,getY()+2,piece));
				}
			} else if (canMove(targetX, targetY)) {
				getMove().add(new Movement(getX(),getY(),targetX,targetY));
			}
		}
		targetY = getY() - 1;
		if (getBoard().isLegalPosition(targetX, targetY)) {
			Piece piece = getBoard().getPieceAtPosition(targetX, targetY);
			if (piece != null) {
				if (canAttack(piece)) {
					getAttack().add(new AttackMovement(getX(),getY(),getX()+2,getY()-2,piece));
				}
			} else if (canMove(targetX, targetY)) {
				getMove().add(new Movement(getX(),getY(),targetX,targetY));
			}
		}
		targetX = getX() - 1;
		if (getBoard().isLegalPosition(targetX, targetY)) {
			Piece piece = getBoard().getPieceAtPosition(targetX, targetY);
			if (piece != null) {
				if (canAttack(piece)) {
					getAttack().add(new AttackMovement(getX(),getY(),getX()-2,getY()-2,piece));
				}
			} else if (canMove(targetX, targetY)) {
				getMove().add(new Movement(getX(),getY(),targetX,targetY));
			}
		}
		targetY = getY() + 1;
		if (getBoard().isLegalPosition(targetX, targetY)) {
			Piece piece = getBoard().getPieceAtPosition(targetX, targetY);
			if (piece != null) {
				if (canAttack(piece)) {
					getAttack().add(new AttackMovement(getX(),getY(),getX()-2,getY()+2,piece));
				}
			} else if (canMove(targetX, targetY)) {
				getMove().add(new Movement(getX(),getY(),targetX,targetY));
			}
		}
	}
	
	public boolean canAttack(Piece p) {
		int dirX = getX() - p.getX();
		int dirY = getY() - p.getY();

		int targetPosX = getX() + dirX * -2;
		int targetPosY = getY() + dirY * -2;

		return (dirX == 1 || dirX == -1) && (dirY == 1 || dirY == -1) &&
				getColor() != p.getColor() &&
				getBoard().isLegalPosition(targetPosX, targetPosY) &&
				getBoard().getPieceAtPosition(targetPosX, targetPosY) == null;
	}
	
	public boolean canMove(int posX, int posY) {
		int targetY;
		int targetX;

		if (getColor() == Piece.BLACK) {
			targetX = getX() + 1;
		} else {
			targetX = getX() - 1;
		}

		if (posY == getY() + 1) {
			targetY = getY() + 1;
		} else if (posY == getY() - 1) {
			targetY = getY() - 1;
		} else {
			return false;
		}

		return (posX == targetX) &&
				getBoard().isLegalPosition(targetX, targetY) && 
				getBoard().getPieceAtPosition(targetX, targetY) == null;
	}
}

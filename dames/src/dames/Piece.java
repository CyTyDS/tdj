package dames;

import java.util.LinkedList;

public abstract class Piece {
	static int WHITE = 0;
	static int BLACK = 1;
	
	private int color;
	private boolean isGhost;
	private int x;
	private int y;
	private LinkedList<AttackMovement> attack;
	private LinkedList<Movement> move;
	private GameModel board;

	public Piece (int col, int posx, int posy, GameModel b) {
		/*if (col != WHITE || col != BLACK) {
			throw new Exception();
		}*/
		color = col;
		isGhost = false;
		x = posx;
		y = posy;
		attack = new LinkedList<AttackMovement>();
		move = new LinkedList<Movement>();
		board = b;
	}
	
	public int getColor() {
		return color;
	}
	
	public boolean getIsGhost() {
		return isGhost;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public void setX(int posX) {
		x = posX;
	}
	
	public void setY(int posY) {
		y = posY;
	}
	
	public LinkedList<AttackMovement> getAttack() {
		return attack;
	}
	
	public LinkedList<Movement> getMove() {
		return move;
	}
	
	public GameModel getBoard() {
		return board;
	}
	
	abstract void possibleMovements();
	
	abstract boolean canAttack(Piece p);
	
	abstract boolean canMove(int x, int y);
	
	public void doMove(Movement m) {
		if (/*attack.contains(m)*/ m.getClass() == AttackMovement.class) {
			Piece target = ((AttackMovement) m).getTarget();
			board.deletePieceAtPosition(target.getX(), target.getY());
			board.movePiece(this, m.getEndX(), m.getEndY());
		} else if (move.contains(m)) {
			board.movePiece(this, m.getEndX(), m.getEndY());
		}
		attack.clear();
		move.clear();
	}
}

package dames;

public class Movement {
	private int beginX;
	private int beginY;
	private int endX;
	private int endY;
	
	public Movement(int bX, int bY, int eX, int eY) {
		beginX = bX;
		beginY = bY;
		endX = eX;
		endY = eY;
	}
	
	public int getBeginX() {
		return beginX;
	}
	
	public int getBeginY() {
		return beginY;
	}
	
	public int getEndX() {
		return endX;
	}
	
	public int getEndY() {
		return endY;
	}

	public Movement clone() {
		return new Movement(beginX, beginY, endX, endY);
	}
}

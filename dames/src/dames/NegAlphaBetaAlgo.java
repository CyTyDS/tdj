package dames;

import java.util.LinkedList;


/**
 * L'idée de la classe est d'implémenter l'algorithme NegAlphaBeta.
 * La méthode play appelle l'algo, qui va renvoyer une valeur (pas très interessante), mais va aussi remplacer bestBoard par le meilleur board que l'on ai trouvé durant negalphabeta.
 * Une fois cela fait, nous n'aurons plus qu'à faire dans Game une sorte de :
 * 		NegAlphaBetaAlgo algo = new NegAlphaBetaAlgo();
 * 		algo.play();
 * 		this.model = algo.getBestBoard();
 */
public class NegAlphaBetaAlgo extends Algorithm {
	
	//Représentation des infinis
	private final int INFINITEPLUS = 100000;
	private final int INFINITENEG = -100000;
	
	//Représentent le meilleur plateau trouvé et sa valeur
	private GameModel bestBoard;
	private int bestNegAlphaBeta;

	//Rappels des ATTR de Algo
//	private int prof;
//	private GameModel board;
//  private HashMap<GameModel, LinkedList<GameModel>> tree;
	
	public NegAlphaBetaAlgo(GameModel b, int prof) {
		super(b, prof);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void play() {
		this.negalphabeta(this.getBoard().clone(), this.INFINITENEG, this.INFINITEPLUS, this.getProf());
	}
	
	/**
	 * Source du code :
	 * https://fr.wikipedia.org/wiki/%C3%89lagage_alpha-b%C3%AAta#Pseudocode
	 * Version Jeu de dames
	 * La classe applique negalphabeta, nous renvoie la valeur negalphabeta de l'arbre (inutile) et remplace bestBoard
	 * 
	 * Il reste des choses à faire : 
	 * Quand on est au niveau de profondeur 0 (prof == this.prof), l'algo doit remplacer bestBoard et bestNegAlphaBeta par la meilleure valeur qu'il existe (dépend du joueur).
	 */
	private int negalphabeta(GameModel noeud, int alpha, int beta, int prof) {
		int v = 0;
		//Si noeud est une feuille
		if (this.getTree().get(noeud) == null) {
			//Si l'on est pas à la bonne profondeur, on crée le noeud
			if (prof != 0) {
				noeud.optimalPath();
				if (!noeud.getAttackPaths().isEmpty()) {
					//pour toutes les possibilités de noeud (ie attaques possibles)
					for (LinkedList<AttackMovement> am : noeud.getAttackPaths()) {
						GameModel node = createNode(noeud, am);
						return -negalphabeta(node, -alpha, -beta, prof);
					}
				} else {
					//pour toutes les possibilités de noeud (ie mouvements possibles)
					for (Piece p : noeud.getPiecesByColor(noeud.getCurrentPlayer())) {
						//pour chaque chemin de la piece
						p.possibleMovements();
						for (Movement m : p.getMove()) {
							GameModel node = createNode(noeud, m);
							//la profondeur est deja ok
							return -negalphabeta(node, -alpha, -beta, prof);
						}
					}
				}
				///////////////////////////////////////////////
				//AUCUNE IDEE DE SI C'EST CE QU'IL FAUT FAIRE//
				///////////////////////////////////////////////
			} else {
				this.bestBoard = noeud;
				if (noeud.getCurrentPlayer() == Piece.WHITE) {
					this.bestNegAlphaBeta = beta;
				} else {
					this.bestNegAlphaBeta = alpha;
				}
			}
			
			return this.calculateHeuristic();
		} else {
			//Si noeud est de type Min == Blanc
			if (noeud.getCurrentPlayer() == Piece.WHITE) {
				v = this.INFINITENEG;
				//pour tout fils de noeud faire
				for (GameModel fils : this.getTree().get(noeud)) {
					v = Math.max(v, -this.negalphabeta(fils, -alpha, -beta, prof - 1));
					if (alpha >= v) { //coupure alpha
						return v;
					}
					alpha = Math.max(alpha,  v);
                }
                beta = Math.max(-beta, -alpha);
			}
			//Si noeud est de type Max == Noir
			else {
				v = this.INFINITENEG;
				for (GameModel fils : this.getTree().get(noeud)) {
					v = Math.max(v, -this.negalphabeta(fils, -alpha, -beta, prof - 1));
					if (v >= beta) { //coupure beta
						return v;
					}
					alpha = Math.max(alpha, v);
                }
                beta = Math.max(-beta, -alpha);
			}
		}
		
		return v;
	}

	/**
	 * Crée le GameModel après le déplacement (Movement) m
	 * Le GameModel subit le mouvement selon le player courrant
	 * @param noeud
	 * @param m
	 * @return
	 */
	private GameModel createNode(GameModel noeud, Movement m) {
		GameModel res = noeud.clone();
		res.getBoard()[m.getBeginX()][m.getBeginY()].doMove(m);
		return res;
	}

	/**
	 * Crée le GameModel après le déplacement (AttackMovement) am
	 * Le GameModel subit la liste de mouvement de la liste am
	 * @param noeud
	 * @param am
	 * @return
	 */
	private GameModel createNode(GameModel noeud, LinkedList<AttackMovement> am) {
		GameModel res = noeud.clone();
		for(AttackMovement m: am) {
			res.getBoard()[m.getBeginX()][m.getBeginY()].doMove(m);
		}
		return res;
	}

	public GameModel getBestBoard() {
		return bestBoard;
	}

	public void setBestBoard(GameModel bestBoard) {
		this.bestBoard = bestBoard;
	}

	public int getBestNegAlphaBeta() {
		return bestNegAlphaBeta;
	}

	public void setBestNegAlphaBeta(int bestNegAlphaBeta) {
		this.bestNegAlphaBeta = bestNegAlphaBeta;
	}
}
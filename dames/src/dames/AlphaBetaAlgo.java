package dames;

import java.util.LinkedList;


/**
 * L'idée de la classe est d'implémenter l'algorithme AlphaBeta.
 * La méthode play appelle l'algo, qui va renvoyer une valeur (pas très interessante), mais va aussi remplacer bestBoard par le meilleur board que l'on ai trouvé durant alphabeta.
 * Une fois cela fait, nous n'aurons plus qu'à faire dans Game une sorte de :
 * 		AlphaBetaAlgo algo = new AlphaBetaAlgo();
 * 		algo.play();
 * 		this.model = algo.getBestBoard();
 */
public class AlphaBetaAlgo extends Algorithm {
	
	//Représentation des infinis
	private final int INFINITEPLUS = 100000;
	private final int INFINITENEG = -100000;
	
	//Représentent le meilleur plateau trouvé et sa valeur
	private GameModel bestBoard;
	private int bestAlphaBeta;

	//Rappels des ATTR de Algo
//	private int prof;
//	private GameModel board;
//  private HashMap<GameModel, LinkedList<GameModel>> tree;
	
	public AlphaBetaAlgo(GameModel b, int prof) {
		super(b, prof);
		this.bestAlphaBeta = this.getRootColor() == Piece.BLACK ? -10000 : 10000;
		this.bestBoard = this.getBoard();
	}

	@Override
	public void play() {
		this.alphabeta(this.getBoard().clone(), this.INFINITENEG, this.INFINITEPLUS, this.getProf());
	}
	
	/**
	 * Source du pseudo-code :
	 * https://fr.wikipedia.org/wiki/%C3%89lagage_alpha-b%C3%AAta#Pseudocode
	 * Version Jeu de dames
	 * La classe applique alphabeta, nous renvoie la valeur alphabeta de l'arbre (inutile) et remplace bestBoard
	 * 
	 * Il reste des choses à faire : 
	 * Quand on est au niveau de profondeur 0 (prof == this.prof), l'algo doit remplacer bestBoard et bestAlphaBeta par la meilleure valeur qu'il existe (dépend du joueur).
	 */
	private int alphabeta(GameModel noeud, int alpha, int beta, int prof) {
		int v = 0;
		//Si noeud est une feuille
		if (prof == 0) {
			return this.calculateHeuristic();
		} else {
			//Si noeud est de type Min == Blanc
			if (noeud.getCurrentPlayer() == Piece.WHITE) {
				v = this.INFINITEPLUS;
				//On crée un noeud, on le teste, on coupe si jamais il y a coupure alpha, sinon on crée le noeud suivant et on teste, etc
				noeud.optimalPath();
				if (!noeud.getAttackPaths().isEmpty()) {
					//pour toutes les possibilités de noeud (ie attaques possibles)
					for (LinkedList<AttackMovement> am : noeud.getAttackPaths()) {
						GameModel fils = createNode(noeud, am);
						if (this.getTree().get(noeud) == null) {
							LinkedList<GameModel> ll = new LinkedList<GameModel>();
							ll.add(fils);
							this.getTree().put(noeud, ll);
						} else {
							this.getTree().get(noeud).add(fils);
						}
						v = Math.min(v, this.alphabeta(fils, alpha, beta, prof - 1));
						if (alpha >= v) { //coupure alpha
							return v;
						}
						beta = Math.min(beta,  v);
					}
				} else {
					//pour toutes les possibilités de noeud (ie mouvements possibles)
					for (Piece p : noeud.getPiecesByColor(noeud.getCurrentPlayer())) {
						//pour chaque chemin de la piece
						p.possibleMovements();
						LinkedList<Movement> mov = GameModel.cloneMovementList(p.getMove());
						for (Movement m : mov) {
							GameModel fils = createNode(noeud, m);
							if (this.getTree().get(noeud) == null) {
								LinkedList<GameModel> ll = new LinkedList<GameModel>();
								ll.add(fils);
								this.getTree().put(noeud, ll);
							} else {
								this.getTree().get(noeud).add(fils);
							}
							//la profondeur est deja ok
							v = Math.min(v, this.alphabeta(fils, alpha, beta, prof - 1));
							if (alpha >= v) { //coupure alpha
								return v;
							}
							beta = Math.min(beta,  v);
						}
					}
				}
			}
			//Si noeud est de type Max == Noir
			else {
				v = this.INFINITENEG;
				//On crée un noeud, on le teste, on coupe si jamais il y a coupure alpha, sinon on crée le noeud suivant et on teste, etc
				noeud.optimalPath();
				if (!noeud.getAttackPaths().isEmpty()) {
					//pour toutes les possibilités de noeud (ie attaques possibles)
					for (LinkedList<AttackMovement> am : noeud.getAttackPaths()) {
						GameModel fils = createNode(noeud, am);
						if (this.getTree().get(noeud) == null) {
							LinkedList<GameModel> ll = new LinkedList<GameModel>();
							ll.add(fils);
							this.getTree().put(noeud, ll);
						} else {
							this.getTree().get(noeud).add(fils);
						}
						v = Math.max(v, this.alphabeta(fils, alpha, beta, prof - 1));
						//SI le alpha devient meilleur à la racine, on change le meilleur chemin
						if (prof == this.getProf()) {
							if (v >= alpha) {
								this.setBestAlphaBeta(v);
								this.setBestBoard(fils);
							}
						}
						if (v >= beta) { //coupure beta
							return v;
						}
						alpha = Math.max(alpha, v);
					}
				} else {
					//pour toutes les possibilités de noeud (ie mouvements possibles)
					for (Piece p : noeud.getPiecesByColor(noeud.getCurrentPlayer())) {
						//pour chaque chemin de la piece
						p.possibleMovements();
						LinkedList<Movement> mov = GameModel.cloneMovementList(p.getMove());;
						for (Movement m : mov) {
							GameModel fils = createNode(noeud, m);
							if (this.getTree().get(noeud) == null) {
								LinkedList<GameModel> ll = new LinkedList<GameModel>();
								ll.add(fils);
								this.getTree().put(noeud, ll);
							} else {
								this.getTree().get(noeud).add(fils);
							}
							//la profondeur est deja ok
							v = Math.max(v, this.alphabeta(fils, alpha, beta, prof - 1));
							if (prof == this.getProf()) {
								if (v >= alpha) {
									this.setBestAlphaBeta(v);
									this.setBestBoard(fils);
								}
							}
							if (v >= beta) { //coupure beta
								return v;
							}
							alpha = Math.max(alpha, v);
						}
					}
				}
			}
		}
		
		return v;
	}

	/**
	 * Crée le GameModel après le déplacement (Movement) m
	 * Le GameModel subit le mouvement selon le player courrant
	 * @param noeud
	 * @param m
	 * @return
	 */
	private GameModel createNode(GameModel noeud, Movement m) {
		GameModel res = noeud.clone();
		res.optimalPath();
		res.getBoard()[m.getBeginX()][m.getBeginY()].doMove(m);
		res.changeCurrentPlayer();
		return res;
	}

	/**
	 * Crée le GameModel après le déplacement (AttackMovement) am
	 * Le GameModel subit la liste de mouvement de la liste am
	 * @param noeud
	 * @param am
	 * @return
	 */
	private GameModel createNode(GameModel noeud, LinkedList<AttackMovement> am) {
		GameModel res = noeud.clone();
		res.optimalPath();
		for(AttackMovement m: am) {
			res.getBoard()[m.getBeginX()][m.getBeginY()].doMove(m);
		}
		res.changeCurrentPlayer();
		return res;
	}

	public GameModel getBestBoard() {
		return bestBoard;
	}

	public void setBestBoard(GameModel bestBoard) {
		this.bestBoard = bestBoard;
	}

	public int getBestAlphaBeta() {
		return bestAlphaBeta;
	}

	public void setBestAlphaBeta(int bestAlphaBeta) {
		this.bestAlphaBeta = bestAlphaBeta;
	}
}
